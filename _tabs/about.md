---
title: About
icon: fas fa-info-circle
order: 4
---

> 방문해 주셔서 감사합니다.
{: .prompt-tip }

이 블로그에는 제가 한 삽질 경험을 정리하여 공유하려고 합니다. 
그 경험들이 다른 사람들에게 도움이 되었으면 좋겠습니다.