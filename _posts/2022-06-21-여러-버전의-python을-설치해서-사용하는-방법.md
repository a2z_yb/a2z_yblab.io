﻿---
title: 여러 버전의 python을 설치해서 사용하는 방법
author: a2z_yb
date: 2022-06-21 09:00:00 +0900
categories: [python]
tags: [python]
---

# 여러 버전의 python을 설치해서 사용하는 방법

WSL  Ubuntu를 설치하면 기본적으로 Python이 설치되어 있습니다.

![img 1](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img01.png)
![img 2](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img02.png)

Ubuntu 20.04 LTS 버전에는 Python  3.8.10 버전이, Ubuntu 22.04 LTS 버전에는 Python  3.10.4 버전이 설치되어 있네요.

설치된 python을 그냥 사용하면 됩니다.

python2와 python3은  많은 부분에서 서로 호환이 안되지만, python3.X 버전 사이에서 상위 버전은 하위 버전의 대부분을 호환합니다.

하지만, 낮은 버전을 사용할 때 상위 버전에서 추가된 새로운 문법이 적용되지 않을 수 있고, 하위 버전의 문법이 deprecated 되어 사용할 수 없을 수도 있는 등 간혹 python 버전이 달라 문법이나 API가 호환되지 않는 문제가 발생하기도 합니다.

이럴 경우 진행하는 프로젝트에 필요한 python 버전에 맞춰 추가로 설치해주시면 됩니다.
> 물론, docker를 이용하는 것이 더 간단할 수 있습니다.

이 글에서는 여러 버전의 python을 설치해서 사용하는 방법을 소개해 드리도록 하겠습니다.
<br />
<br />

## 1. Python 버전 추가 설치

설치된 python 외에 다른 버전의 python을 추가로 설치해보겠습니다.

$ sudo apt install python3.X 의  방법으로도 설치할 수 있지만, 환경이 꼬일 수 있기 때문에 소스 코드로부터 직접 빌드해보도록 하겠습니다.

예를 들어, **python 3.7.13** 버전을 추가로 설치해보겠습니다.

![img 3](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img03.png)

먼저, 다음 링크([https://www.python.org/downloads/](https://www.python.org/downloads/))에 접속합니다.

원하는 버전을 찾아 **Download**를 누릅니다.

![img 4](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img04.png)
![img 5](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img05.png)

아래의 Files에서 마우스 오른쪽 버튼을 눌러 소스 코드의 링크를 복사합니다.

![img 6](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img06.png)

윈도우 터미널에서 다음 명령어로 빌드에 사용할 폴더를 생성하고 그 폴더로 이동합니다.

```bash
mkdir  py_3.7.13
```
```bash
cd py_3.7.13
```

![img 7](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img07.png)

좀전에 복사한 링크로부터 압축 파일을 다운 받습니다.

```bash
wget https://www.python.org/ftp/python/3.7.13/Python-3.7.13.tgz
```

![img 8](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img08.png)
![img 9](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img09.png)

압축을 풀고 그 폴더로 이동합니다.

```bash
tar -xvzf  Python-3.7.13.tgz
```
```bash
cd Python-3.7.13.tgz
```

![img 10](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img10.png)

빌드에 필요한 패키지를 설치하겠습니다.

```bash
sudo apt dist-upgrade
```
```bash
sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev
```

![img 11](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img11.png)

빌드해보겠습니다,

```bash
./configure
```

위 과정에서 종속성 오류가 뜨는지 잘 살펴보세요. 만약, 오류가 발생했다면 해결하고 넘어가야 정상적인 빌드가 가능합니다.

![img 12](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img12.png)

```bash
sudo make altinstall
```

그냥 install을 하면 기존의 python을 덮어 씌우는 경우가 있어서 반드시 altinstall을 사용해야 한다고  합니다.

![img 13](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img13.png)

새로 설치한 버전의 python을 실행시켜보겠습니다.
버전을 조금 구체적으로 입력하면 특정 버전의 python을 실행할 수 있습니다.

```bash
python3.7
```

![img 14](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img14.png)

기존의 python을 실행시켜보겠습니다.

```
python3
```

두 버전의 python 모두 잘 실행되는 것을 확인할 수 있습니다.
<br />
<br />

## 2. default python 버전 지정

지금은 python3 명령어로 python을 실행시키면 기존에 설치된 3.10 버전이 실행됩니다.

새로 설치한 버전을 실행시키기 위해선 python3.7 등과 같이 상세 버전을 명시해줘야 합니다.

매번 상세 버전을 기입하여 실행하면 번거롭고 설치된 python 버전이 많으면 헷갈릴 수 있기 때문에 python3 명령어로 특정 버전의 python을 등록하여 그 버전을 실행할 수 있습니다.

![img 15](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img15.png)

먼저, python이 설치된 위치를 확인해보겠습니다.

```bash
which python3.10
```
```bash
which python3.7
```

![img 16](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img16.png)
![img 17](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img17.png)

기존에 설치되어 있던 python의 우선 순위를 2로, 새로 설치한 python의 우선 순위를 1로 버전의 우선 순위를 변경하여 python3 명령어를 입력하면 3.7 버전을 실행시켜보겠습니다.

"sudo update-alternatives --install [simbolic link path] [python] [real path] [priority]" 명령어를 이용하면 됩니다.

```bash
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.10 2
```
```bash
sudo update-alternatives --install /usr/bin/python3 python3 /usr/local/bin/python3.7 1
```

Real path 부분은 좀전에  which 명령어로 확인한 경로를 적어주시고, 마지막 1과 2는 우선 순위가 각각 1과 2임을 의미합니다.

![img 18](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img18.png)

이제 python3 명령어를 입력하면 새로 설치한 3.7 버전이 실행됩니다.

![img 19](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img19.png)

같은 방법으로 python3 명령어가 아닌 python으로 python 3.7 버전을 실행하도록 해보겠습니다.

```bash
sudo update-alternatives –install /usr/bin/python python /usr/local/bin/python3.7 1
```

이제 python3이 아닌 python 명령어로도 python을 실행할 수 있습니다.

![img 20](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/python_version_install/img20.png)

Python의 우선 순위를 등록해 두면, 다음 명령어로 쉽게 등록된 모든 버전의 python을 확인할 수 있고, python3 명령어로 실행하는 버전을 변경할 수 있습니다.

```bash
sudo update-alternatives --config python3
```

원하는 python 버전을 확인하고 Selection의 숫자를 입력하고 enter를 누르면 반영됩니다.
<br />
<br />

## 3. 마무리
이러한 방법으로 여러 버전의 python을 설치하고 사용할 수 있습니다. 😊

참고로, pip을 통해 설치한 python 라이브러리 패키지는 버전 간 설치 경로가 다르기 때문에 버전 별로 설치 및 관리를 해야합니다.

시스템에 여러 버전의 python을 설치하였다면, 환경이 꼬이지 않도록 가상 환경이나 docker 환경에서 작업하는 것을 권장합니다.
