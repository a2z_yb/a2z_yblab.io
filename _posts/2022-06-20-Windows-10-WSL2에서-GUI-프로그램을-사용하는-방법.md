﻿---
title: Windows 10 WSL2에서 GUI 프로그램을 사용하는 방법
author: a2z_yb
date: 2022-06-20 16:30:00 +0900
categories: [개발 환경, Tip]
tags: [WSL, GUI]
---

# Windows 10 WSL2에서 GUI 프로그램을 사용하는 방법

Windows 11에서 WSL2를 사용하면 별도의 작업 없이 GUI 프로그램을 사용할 수 있는 듯 합니다.

하지만, 호환성 등의 이유로 저처럼 Windows 10을 사용해야 하는 상황에서는 아래 방법을 통해 GUI 프로그램을 사용할 수 있습니다.
<br />
<br />

## 1. 설치

![img 1](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img01.png)

Windows에서 다음 [링크](https://sourceforge.net/projects/vcxsrv/) 에 접속하여 **Download** 클릭합니다.

![img 2](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img02.png)

다운받은 설치 파일을 실행해줍니다.

![img 3](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img03.png)

**Next \>** 버튼을 클릭합니다.

![img 4](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img04.png)

**Install** 버튼을 클릭합니다.

![img 5](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img05.png)

**Close** 버튼을 클릭합니다.
<br />
<br />

## 2. 설정

![img 6](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img06.png)

win키를 눌러 **Xlaunch**를 실행합니다.

![img 7](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img07.png)

**다음(N) \>** 버튼을 클릭합니다.

![img 8](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img08.png)

**다음(N) \>** 버튼을 클릭합니다.

![img 9](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img09.png)

그림과 같이 체크하고 **다음(N) \>** 버튼을 클릭합니다.

![img 10](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img10.png)

**Save configuration** 버튼을 눌러 설정 파일을 저장하고 **마침** 버튼을 클릭합니다.

![img 11](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img11.png)

방화벽 관련 설정을 **모두 체크**하고 **액세스 허용**을 클릭합니다.

![img 12](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img12.png)

다음 명령어로 메모장을 열어줍니다.

```bash
notepad.exe  ~/.bashrc
```

.bashrc의  맨 아래 부분에 다음과 같이 입력합니다.

> **1\) 고정 IP를 사용하는 경우**
> 
> ```
> export DISPLAY=XXX.XXX.XXX.XXX:0.0
> ```
> 
> X 자리에 본인의 고정 IP를 입력합니다.
> 
> **2\) 유동(동적) IP를 사용하는 경우**
> 
> ```
> export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0
> ```

Ctrl + s로 저장 하고, 메모장을 닫아줍니다.

![img 13](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img13.png)

다음 명령어로 .bashrc를 적용 시켜줍니다.
```bash
source ~/.bashrc
```

![img 14](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img14.png)

Powershell에서 다음 명령어로 WSL을 종료합니다.

```powershell
wsl --shutdown
```

윈도우 터미널을 실행시켜 WSL을 다시 실행합니다.
<br />
<br />

## 3. 동작 확인

설정이 제대로 됐는지 확인해봅시다.

![img 15](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img15.png)

리눅스에서 텍스트 편집 등을 할 수 있는 gedit을 설치해 줍니다.

```bash
sudo apt install gedit -y
```

![img 16](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img16.png)

설치가  완료되면 gedit을 실행해봅시다.

```bash
gedit
```

![img 17](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img17.png)

잘 됩니다.😁
<br />
<br />

## 4. 사용 팁

![img 18](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img18.png)

WSL에서 GUI를 사용하려면 xlaunch가 먼저 실행되어야만 합니다.

만약  wsl을 자주 이용하지 않는다면, 필요할 때마다 xlaunch를 직접 실행한 후 사용해도 되지만,

저는 GUI 프로그램을 사용할 때마다 매번 따로 실행하는 것이 번거롭기 때문에, 시작 프로그램으로 설정하겠습니다.

![img 19](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img19.png)

![img 20](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img20.png)

윈도우키 + r을 누르고 열기(O):에 shell:startup을 입력하고 확인을 눌러줍니다.

![img 21](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_GUI/img21.png)

2번  과정에서 **Save configuration** 버튼을 눌러 저장한 설정 파일을 시작프로그램 폴더에 복사합니다.

이렇게 해 놓으면 PC가 부팅될 때마다 자동으로 해당 파일이 실행되기 때문에, 별도의 실행 작업 없이 WSL에서 GUI 프로그램을 사용할 수 있습니다.
<br />
<br />

## 5. 마무리

WSL의 리눅스는 기본적으로 CLI로 동작하기때문에 GUI를 이용하다보면 가끔 프로그램이 스스로 꺼지는 등의 오류가 발생합니다. (크게 불편함을 느끼진 않았습니다.)

그리고, 아예 gnome이나 Xfce, KDE 등의 DE(desktop environment)를 설치하여 완전한 GUI desktop으로도 사용할 수도 있는 것 같습니다.

하지만, 소개한 방법만으로도 개인적으로는 만족하고, DE의  경우 무거울 것 같기 때문에 이 정도의 방법만 소개합니다.😊

