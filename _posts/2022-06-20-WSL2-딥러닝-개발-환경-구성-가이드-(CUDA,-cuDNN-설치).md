﻿---
title: WSL2 딥러닝 개발 환경 구성 가이드 (CUDA, cuDNN 설치)
author: a2z_yb
date: 2022-06-20 09:00:00 +0900
categories: [개발 환경, Guide]
tags: [WSL]
---

# WSL2 딥러닝 개발 환경 구성 가이드 (CUDA, cuDNN 설치)

WSL2에서 딥러닝 개발 환경을 구축하기 위하여 CUDA와 cuDNN을 이용하는 두 가지의 방법이 있습니다.

1. WSL2의 리눅스 배포판(Ubuntu)에 설치하여 사용
2. Docker 이용

1번의 방법은 로컬 pc에서 환경을 구성할 때 사용하고, 2번 방법은 리모트 서버에 접속하여 다양한 버전의 딥러닝 프레임워크가 필요할 때 사용하면 편리합니다.

이 글에서는 1번 방법인 WSL2의 Ubuntu에  설치하는 방법 위주로 설명드리고자 합니다. (2번 방법은 추가로 작성 예정)
<br />
<br />

## 0. 설치 버전 확인

제가 설치 버전을 고르는 방법은 먼저, 딥러닝 프레임워크의 버전을 선택한 후, 프레임워크가 지원하는 CUDA 버전을 체크한 다음, CUDA 버전에 맞는 cuDNN을 확인합니다.
<br />

**버전 확인 순서**
> 딥러닝 프레임워크(TensorFlow, Pytorch  등) 설치 버전 확인 -> CUDA 설치 버전 확인 -> cuDNN  설치 버전 확인

**설치 순서**
> (nvidia graphic driver) -> CUDA -> cuDNN -> 딥러닝 프레임워크

딥러닝 개발을 위하여 NVIDIA의 CUDA를 이용하여 GPU 가속을 할 때,  CUDA 버전과 cuDNN의 버전보다 먼저 확인해야 할 것이 딥러닝 프레임워크(TensorFlow, Pytorch  등)의 버전입니다.

CUDA, cuDNN과 딥러닝 프레임워크 간의 버전 호환이 되지 않을 수 있기 때문입니다.

따라서, 환경 구성 전 딥러닝 프레임 워크의 버전을 먼저 확인하고, 이에 맞는 CUDA와 cuDNN을 설치합니다.

이 글에서는 딥러닝 프레임워크로 ‘pytorch 1.8.2 LTS 버전’을 설치하고자 합니다. 따라서, 이 버전이 지원하는 CUDA 11.1 버전을 설치합니다.
<br />
<br />

## 1. NVIDIA  graphic driver 설치

CUDA를 설치하기 전, PC에 NVIDIA graphic drvier가 설치되어 있어야 합니다.

- WSL으로 사용 중이면 윈도우에만 설치하면 됨
- 그 외, 우분투만 또는 듀얼 부팅중이면 우분투에 설치

사용하는 PC에 NVIDIA  graphic  driver가 설치되어 있으면 1-1을, 설치되어 있지 않으면 1-2를 따라가 주세요.
<br />

### 1-1. 이미 NVIDIA graphic driver가 설치되어 있는 경우
Powershell에 nvidia-smi  명령어로 현재 드라이버 버전을 확인합니다.
![img 1](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC1.png)

```powershell
nvidia-smi
```
<br />

![img 2](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC2.png)

작업 표시줄에서 오른쪽에 위치한 **숨겨진 아이콘 표시\(^ 버튼\)**를 클릭합니다.

![img 3](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC3.png)

마우스 **오른쪽** 버튼으로 **GeForce Experience 아이콘**을 클릭하고, **NVIDIA GeForce Experience**를 선택합니다.

![img 4](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC4.png)

NVIDIA 계정에 로그인 합니다.

![img 5](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC5.png)

좌측 상단의 **드라이버** 탭을 누르면, 사용 가능한 NVIDIA driver를 보여줍니다.

![img 6](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC6.png)

업데이트가 있을 경우 **다운로드** 버튼을 클릭합니다.
이미 최신 버전의 드라이버를 사용 중이라면, **2번 과정**으로 넘어가 주세요.

![img 7](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC7.png)

설치가 완료되면, **업데이트 준비 중…**이라는 메시지가 출력됩니다.

![img 8](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC8.png)

**사용자 지정 설치**를 클릭합니다.

![img 9](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC9.png)

**설치 준비 중…**이 뜨면 잠시 기다려 줍니다.

![img 10](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC10.png)

원하는 설치 옵션을 선택한 후 **설치** 버튼을 누릅니다.

![img 11](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC11.png)

설치가 진행되면, 먼저 기존의 드라이버를 삭제하고 새로운 버전의 드라이브가 설치됩니다.

이때, 듀얼 모니터를 사용하신다면 서브 모니터의 연결이 끊어질 수 있으니 당황하지 마세요. 설치가 진행되면서 다시 연결됩니다.

![img 12](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC12.png)

설치가 완료되었으면 **닫기** 버튼을 클릭합니다.

![img 13](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC13.png)

Powershell에 nvidia-smi 명령어를 입력으로 드라이버 버전을 확인하여 업데이트가 제대로 됐는지 볼 수 있습니다.

```powershell
nvidia-smi
```
<br />

**재부팅** 해줍니다.

윈도우에서 NVIDIA driver를 설치를 완료하면 WSL의 우분투에서 별도의 작업없이 NVIDIA driver를 사용하실 수 있습니다.

![img 14](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC14.png)

WSL의 우분투에서도 제대로 적용됐는지 윈도우 터미널에서 다음  명령어로 확인합니다.

```bash
nvidia-smi
```
<br />

위와 같이 뜨지 않는다면, 드라이버 설치 후 **재부팅**  여부를 다시 한번 확인 해주세요.
<br />

### 1-2. 이미 NVIDIA graphic driver가 설치되어 있지 않은 경우
![img 15](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC15.png)

[이곳](https://www.nvidia.co.kr/Download/index.aspx?lang=kr)에서 본인의 PC 환경에 맞는 설정을 선택한 후 **검색**을 클릭해 주세요.
다운로드 타입에서 Game Ready / Studio가 있는데 전자는 게이밍 용 후자는 영상 편집 등 용도로 생각하시면 됩니다.

둘 다 목적이 아니니 아무거나 받아줍니다.

![img 16](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC16.png)
![img 17](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC17.png)

**다운로드** 버튼을 클릭

![img 18](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC18.png)

다운 받은 설치 파일을 실행해줍니다.

![img 19](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC19.png)

**OK** 버튼을 클릭

![img 20](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC20.png)

기다립니다.

설치 과정에서는 본인 취향에 맞게 옵션을 선택해서 설치하시면 됩니다. 저는 모두 설치하겠습니다.

![img 21](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC21.png)

**동의 및 계속(A)** 클릭

![img 22](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC22.png)

**다음(N)** 클릭

![img 23](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC23.png)

기다려주세요.

![img 24](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC24.png)

설치 준비가 끝나면 설치를 진행합니다.
이때 화면이 깜빡거릴 수 있습니다.

![img 25](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC25.png)

설치가 완료되면 **닫기\(Ｃ\)**를 클릭합니다.

![img 26](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC26.png)

Powershell에 다음 명령어를 입력하여 NVIDIA graphic driver가 제대로 설치됐는지 확인합니다.

```powershell
nvidia-smi
```
<br />

**재부팅**해주세요.
<br />
<br />

## 2. CUDA 설치

[CUDA Toolkit Archive](https://developer.nvidia.com/cuda-toolkit-archive)에는 최신 CUDA 버전부터, 이전에 공개된 모든 CUDA 버전을 다운 받을 수 있습니다.

![img 27](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC27.png)

지금은 11.1버전이 필요하니 11.1.X에서 숫자가 높은 버전을 선택합니다.

![img 28](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC28.png)

Operating System은 우분투에 설치할 것이기 때문에 **Linux**를 선택합니다.

![img 29](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC29.png)

Architecture은 **x86_64** 선택합니다.

![img 30](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC30.png)

Distribution은 WSL을 사용하기 때문에 **WSL-Ubuntu**를 선택합니다.

![img 31](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC31.png)

Version은 WSL1에서는 graphic 가속을 사용할 수 없으므로 WSL2을 나타내는 **2.0**으로 자동선택 됩니다.

Installer Type은 **runfile \(local\)**을 선택해 주세요.

![img 32](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC32.png)

위와 같이 선택을 완료하면 설치 명령어를 알려줍니다.
윈도우 터미널에 이 명령어를 순서대로 입력합니다.

![img 33](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC33.png)

다음 명령어로 CUDA 설치 파일을 다운받습니다.

```bash
wget https://developer.download.nvidia.com/compute/cuda/11.1.1/local_installers/cuda_11.1.1_455.32.00_linux.run
```
<br />

![img 34](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC34.png)

다음  명령어로 CUDA 설치 파일을 실행합니다. sudo  명령어 때문에 권한이 필요할 경우 우분투 비밀번호도 입력해줍니다.

```bash
sudo sh cuda_11.1.1_455.32.00_linux.run
```
<br />

![img 35](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC35.png)

명령어 입력 후 조금 기다리면  다음과 같이 설치를 시작합니다.
**accept**를 입력해주세요.

![img 36](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC36.png)

설치할 항목들을 선택합니다.
방향키로 움직이고, 스페이스바로 해제, 선택할 수 있습니다.

CUDA Toolkit은 꼭 체크하시고, 설치를 원하지 않는 부분은 체크 해제해 주세요. 저는 모두 설치하도록 하겠습니다.

설치할 X표시로 체크한 다음, **방향키**를 이용하여 **Install**로 이동 후 **엔터**를 눌러줍니다.

![img 37](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC37.png)

잠시 기다리시면 다음과 같이 출력됩니다.

여기서 WARNING은 이 단계에서 NVIDIA driver를 설치하지 않아서 나타나는 경고인데, 저희는 미리 설치했으니 신경 안쓰셔도 됩니다.

![img 38](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC38.png)

이제, CUDA Toolkit 관련 경로를 환경 변수에 추가하고 적용해야 합니다.

gedit이나 vim 등의 리눅스 텍스트 편집기를 이용해도 되지만,
저는 WSL 이용 중이니 windows의 메모장을 이용하겠습니다.

```bash
notepad.exe ~/.bashrc
```
<br />

![img 39](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC39.png)

메모장이 열리면 맨 아래에 다음의 내용을 추가해주세요.

만약 저와 같은 버전의 CUDA를 설치하는 것이 아니라면, cuda-xx.x에서 x부분에 해당하는 버전을 입력해주세요.
```
export PATH=/usr/local/cuda-11.1/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-11.1/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
export CUDA_HOME=/usr/local/cuda
```
<br />

Ctrl + s로 저장 후 메모장을 닫아줍니다.

![img 40](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC40.png)

다음  명령어로 변경한 .bashrc를 적용시켜 줍니다.

```bash
source ~/.bashrc
```
<br />

![img 41](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC41.png)

이제, CUDA 설치와 설정이 끝났습니다. 다음 명령어로 제대로 설치됐는지 확인할 수 있습니다.

```bash
nvcc --version
```
<br />

---
**Trouble shooting**
![img 42](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC42.png)

$ sudo  sh cuda_11.1.1_455.32.00_linux.run 명령어를 입력했을 때 이러한 오류가 발생한다면, gcc가 설치되어 있지 않은 경우입니다.

![img 43](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC43.png)

다음 명령어로 개발을 위한 필수 프로그램들을 설치해주세요.

```bash
sudo apt install build-essential –y
```
<br />

설치가 완료되면, 다시 $ sudo  sh cuda_11.1.1_455.32.00_linux.run 명령어를 입력합니다.

---
<br />
<br />

## 3. cuDNN  설치

다음은  cuDNN  설치 방법입니다.

[https://developer.nvidia.com/cudnn](https://developer.nvidia.com/cudnn)  에 접속합니다.

![img 44](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC44.png)

**Download  cuDNN \>**을 클릭합니다.

![img 45](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC45.png)

NVIDIA developer 계정이 필요합니다.
계정이 있으신 분은 **Login**을, 없으신 분은 **Join now**로 가입해줍니다.

![img 46](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC46.png)

체크하여 라이센스에 동의합니다.
뜨는 최신 버전 말고, 아래에 있는 **Archived cuDNN Releases**를 클릭합니다.

![img 47](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC47.png)

설치한 CUDA 버전에 대응하는 cuDNN을 찾아 다운을 받아야 합니다.

제가 설치한 CUDA 11.1에 해당하는 cuDNN  버전이 여러 개가 있는데, 저는 v8.1.1 버전을 설치해 보겠습니다.

![img 48](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC48.png)

해당하는 다양한 설치 목록들이 있는데, 여기서 cuDNN Library for Linux (x86_64) 다운 받았습니다.

![img 49](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC49.png)

다운로드가 완료되면, 윈도우 터미널에서 다음 명령어로 윈도우 탐색기를 열어줍니다.

```bash
explorer.exe .
```
<br />

![img 50](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC50.png)

다운 받은 압축 파일을 복사해옵니다.

![img 51](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC51.png)

윈도우 터미널에서, 다음 명령어로 압축을 풀어줍니다.

```bash
tar -xzvf cudnn-11.2-linux-x64-v8.1.1.33.tgz
```
<br />

![img 52](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC52.png)

다음 명령어를 순서대로 입력하여 설치를 진행합니다.

```bash
sudo cp cuda/include/cudnn*.h /usr/local/cuda/include
```
```bash
sudo cp -P cuda/lib64/libcudnn* /usr/local/cuda/lib64
```
```bash
sudo chmod a+r /usr/local/cuda/include/cudnn*.h /usr/local/cuda/lib64/libcudnn*  
```
<br />

별도로 출력되는 내용은 없으며, 우분투 사용자 비밀번호를 요구하면 입력해줍니다.

![img 53](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC53.png)
![img 54](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC54.png)

cuDNN  설치가 완료되었으니, 제대로 설치 됐는지 확인해 봅니다.

```bash
cat /usr/local/cuda/include/cudnn_version.h | grep CUDNN_MAJOR -A 2
```
```bash
ldconfig -N -v $(sed 's/:/ /' <<< $LD_LIBRARY_PATH) 2>/dev/null | grep libcudnn
```
<br />

위와 같이 출력된다면, 제대로 설치가 완료된 것입니다.👍
<br />
<br />

## 4. 딥러닝 프레임워크 설치

![img 55](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC55.png)

먼저, python 개발 종속 패키지와 패키지 관리자를 설치합니다.

```bash
sudo apt install python3-dev python3-pip
```
<br />

**1) Pytorch**

다음 [링크](https://pytorch.org/get-started/locally/)에 접속하여 설치할 버전과 명령을 확인합니다.

![img 56](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC56.png)

예로, Pytorch 1.8.2 LTS 버전을 Windows OS에서 패키지 관리자로 pip을 사용하고, python 언어를 이용하며, CUDA 11.1 버전이  설치되어 있으면, 아래의 명령어로 설치할 수 있습니다.

![img 57](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC57.png)

```bash
pip3 install torch==1.8.2 torchvision==0.9.2 torchaudio===0.8.2 --extra-index-url https://download.pytorch.org/whl/lts/1.8/cu111
```
<br />

![img 58](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC58.png)

설치가 완료되면, pytorch가 제대로 설치가 됐는지와  프레임워크에서 CUDA를 이용할 수 있는지 확인해 봅니다.

```bash
python3
```
```python-repl
>>> Import torch
>>> torch.__version__
‘1.8.2+cu111’
>>> torch.cuda.is_available()
True
>>> exit()
```
<br />

**1.8.2+cu111**과 **True**가 출력된다면 올바르게 동작하고 있는 것입니다.😄
<br />

**2) Tensorflow**

다음 [링크](https://www.tensorflow.org/install/source#docker_linux_builds)에서 설치할 Tensorflow와 python, CUDA, cuDNN  버전을 확인합니다.

![img 59](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC59.png)

Tensorflow  공식 홈페이지에 있는 **테스트된 빌드 구성**에서 지금 설치된 CUDA 11.1 버전과 정확히 일치한 것이 없지만, 일단 설치해 보겠습니다.

이런 경우, 제대로 동작하지 않을 수 있습니다. 가능하다면 '테스트된 빌드 구성'을 따라주세요.

![img 60](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC60.png)

tensorflow==x.x.x  으로  원하는 버전을 선택하여 설치할 수 있습니다. ==옵션이 없으면 최신 버전의 tensorflow가 설치됩니다.

```bash
pip3  install  tensorflow
```
<br />

![img 61](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL_CUDA_cuDNN/%EA%B7%B8%EB%A6%BC61.png)

다음 명령어로 tensorflow에서 gpu를 사용할 수 있는지 확인해봅니다.

```bash
python3
```
```python-repl
>>> import tensorflow as tf
>>> tf.__version__
'2.9.1’
>>> print(tf.test.is_built_with_cuda())
True
>>> print(tf.test.is_built_with_gpu_support())
True
>>> from tensorflow.python.client import device_lib
>>> device_lib.list_local_devices()

…

[name: "/device:CPU:0"
device_type: "CPU"
memory_limit: 268435456
locality {
}
incarnation: 17653532043727274633
xla_global_id: -1
, name: "/device:GPU:0"
device_type: "GPU"
memory_limit: 6227951616
locality {
    bus_id: 1
    links {
    }
}
incarnation: 8102655611067132252
physical_device_desc: "device: 0, name: NVIDIA GeForce RTX 2070 SUPER, pci bus id: 0000:01:00.0, compute capability: 7.5"
xla_global_id: 416903419
]
```
<br />

만약, CPU 관련 내용만 나온다면 GPU를 사용할 수 없는 상태입니다. 이와 달리, CPU와 GPU에 대해 모두 출력된다면, CUDA를 사용할 수 있습니다.

